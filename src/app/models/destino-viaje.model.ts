import {v4 as uuid} from 'uuid';

export class DestinoViaje {

    private selected:boolean;
    public servicios:string[];
    public id = uuid();

    constructor(public nombre:string, public imagenUrl:string, public descripcion:string, public votes: number = 0) {
        this.servicios = ['Piscina', 'Desayuno', 'Cena'];
    }

    isSelected() : boolean {
        return this.selected;
    }

    setSelected(s:boolean) {
        this.selected = s;
    }

    voteUp() {
        this.votes++;
    }

    voteDown() {
        this.votes--;
    }

}